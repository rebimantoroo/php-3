<?php
//require('animal.php');
require('Frog.php');
require('Ape.php');

$sheep = new Animal("shaun");
echo 'Name : ' . $sheep->name . '<br>';
echo 'Legs : ' . $sheep->legs . '<br>';
echo 'cold blooded : ' . $sheep->cold . '<br><br>';

$kodok = new frog("budu");
echo 'Name : ' . $kodok->name . '<br>';
echo 'Legs : ' . $kodok->legs . '<br>';
echo 'cold blooded : ' . $kodok->cold . '<br>';
$kodok->jump();


$sungkong = new ape("kera sakti");
echo '<br><br>Name : ' . $sungkong->name . '<br>';
echo 'Legs : ' . $sungkong->legs . '<br>';
echo 'cold blooded : ' . $sungkong->cold . '<br>';
$sungkong->yell();
